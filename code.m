function code(norad)
%% > Inputs: norad
%       - norad = 0; Read all last Starlink Satellites TLEs log
%       - norad = any number ; Read all availables TLEs from one specific satellite 
% 
% > Outputs: KPE, graphs 
%       - Keplerian coordinates of the satellites
%       - Graphs of the specified variables

% Get .txt file
if norad <= 0 % Import directly from web all starlink available TLE
    url = urlread('http://celestrak.com/NORAD/elements/starlink.txt');
    
    file = fopen('starlink.txt','w');
    fprintf(file,url);
    fclose(file);
    
    file = 'starlink.txt';
    fd = fopen(file,'r');
else  % Import just one satellite TLE logs over time (TLE with API Key in https://www.space-track.org/#/g)
    cd norad
    name = dir(append(num2str(norad),'.txt'));
    
    if isempty(name) == 1
        cd ..
        tle2txt(norad)
        fprintf('Looking for file on the web')
    else
        cd ..
        fprintf('File already created, using it')
    end
    
    file = append('norad/',num2str(norad),'.txt');
    fd = fopen(file,'r');
end

% Line counting for declaring variables to plot
count = 0;
for i=1:1000000
    line = fgetl(fd);
    if line == -1
        break
    else
        count = count + 1;
    end
end

if norad <= 0
    items=count/3;
else
    items=count/2;
end

% All the variables to plot
incl_acc    = zeros(1,items); % Inclination
omega_acc   = zeros(1,items); % RA of ascending node
ecc_acc     = zeros(1,items); % Eccentricity
w_acc       = zeros(1,items); % Argument of perigee
M_acc       = zeros(1,items); % Mean anomaly
n_acc       = zeros(1,items); % Mean motion
T_acc       = zeros(1,items); % Orbital Period
a_acc       = zeros(1,items); % Semi-major axis
b_acc       = zeros(1,items); % Semi-minor axis
satnum_acc = zeros(1,items);

% Read the .txt file and compute
if norad <= 0
    %read3tle(file)
    %tle2kep(file)
    tle2kep(file)
    
    load('data')% (add the variables to plot in readtle.m file, line 69)
    % Plots of the desired results
    figure()
    hist(a_acc); title('Altitude Starlink'); xlabel('Altitude of the orbit [km]'); ylabel('Number of satellites');ylim([0 1100])
    saveas(gcf,append('starlink_altitude.png'));

    figure()
    hist(ecc_acc); title('Eccentricity Starlink'); xlabel('Eccentricity'); ylabel('Number of satellites');ylim([0 1100])
    figure()
    hist(incl_acc); title('Inclination Starlink'); xlabel('Inclination [deg]'); ylabel('Number of satellites');ylim([0 1100])
    saveas(gcf,append('starlink_inclination.png'));

    
else
    %readtle(file)
    tle2kep(file)
    load('data')% (add the variables to plot in readtle.m file, line 69)
    epoch19_acc = epoch19(epoch_acc);
    % Plots of the desired results
    figure()
    plot(epoch19_acc, a_acc);title(append('Altitude Starlink No.: ',num2str(satnum))); ylabel('Altitude of the orbit [km]'); xlabel('Days since first launch (24-May-2019)');
    saveas(gcf,append('starlink_plots\altitude\',num2str(satnum),'_altitude.png'));
    %close;
%     figure()
%     plot(epoch19_acc, ecc_acc); title(append('Eccentricity Starlink No.: ',num2str(satnum))); ylabel('Eccentricity'); xlabel('Days since 2019 Jan. 1^{st}');
%     figure()
%     plot(epoch19_acc, incl_acc); title(append('Inclination Starlink No.: ',num2str(satnum))); ylabel('Inclination [deg]'); xlabel('Days since 2019 Jan. 1^{st}');
end
end