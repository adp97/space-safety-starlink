%% Input: 
% - norads: vector of norad to correct
% 
% Output:
% - SMA outliers removed and corrected
function outliers_out(norads,tol)
for i=1:length(norads)
    code(norads(i));
    load('data.mat');
    n=length(a_acc);
    
    % Smooth Interpolation
    a_smooth = smoothdata(a_acc,'rlowess',20);
    subplot(2,2,1)
    plot(a_acc,'b','LineWidth',2); hold on; plot(a_smooth,'r--','LineWidth',2); title(append('Smooth Interpolation Starlink No.: ',num2str(norads(i))))
    legend('Real value','Smooth value','Location','best'); ylabel('Altitude of the orbit [km]'); xlabel('TLE No.');
    %saveas(gcf,append('starlink_plots\altitude\',num2str(satnum),'_smooth_altitude.png'));
     
    
    % Outliers detector
    error = abs(a_acc-a_smooth)./a_smooth;
    epoch = epoch19(epoch_acc);
    subplot(2,2,2)
    plot(error);title(append('Relative Error Starlink No.: ',num2str(norads(i)))); xlabel('TLE No.'); ylabel('Error');
    hold on; yline(tol,'r--'); legend('Error',append('Tolerance error: ',num2str(tol)))
    %saveas(gcf,append('starlink_plots\altitude\',num2str(satnum),'_error_det_altitude.png'));
    
    % Plot of previous data as dots *
    subplot(2,2,3)
    plot(epoch,a_acc,'b*','MarkerSize',5); title(append('Altitude Starlink No.: ',num2str(norads(i))));
    ylabel('Altitude of the orbit [km]'); xlabel('Days since first launch (24-May-2019)'); hold on
    
    % Plot error data
    is_error = error>tol;
    plot(epoch,1.05*max(a_acc)*is_error,'r--'); ylim([min(a_acc) 1.01*max(a_acc)]);
    %saveas(gcf,append('starlink_plots\altitude\',num2str(satnum),'_error_altitude.png'));
    
    % Final plot without error values
    not_error = not(is_error);
    subplot(2,2,4)
    plot(epoch(not_error),a_acc(not_error),'LineWidth',2);title(append('Corrected Altitude Starlink No.: ',num2str(norads(i))));
    ylabel('Altitude of the orbit [km]'); xlabel('Days since first launch (24-May-2019)'); %xlim([0 epoch19(21000+day(datetime(date),'dayofyear'))])
    %saveas(gcf,append('starlink_plots\altitude\',num2str(satnum),'_corrected_altitude.png'));
end
end

