%% Function to refers an epoch YYDDD.DDDD to elapsed days from 1st Jan. 2019
%
function elapseddays = epoch19(epoch)
    if epoch < 19000
       error('Epoch input is previous to 1st Jan 2019'); 
    end
    elapseddays = zeros(1,length(epoch));
for i = 1:length(epoch)
    year = floor(epoch(i)/1000);
    years = 2000 + linspace(19,year,1+year-19);
    leap_years = sum(leapyear(years));
    days = epoch(i) - year*1000;
    
    elapseddays(i) = 365*(year-19) + days + leap_years - 1 - day(datetime('24-May-2019'),'dayofyear');

end