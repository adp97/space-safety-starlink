function nu = M2nu(M,ecc)
tol = 10^-8;
E = CalcEA(M,ecc,tol);  
nu = atan2((sin(E)*(1-ecc^2)^.5),(cos(E)-ecc));
end