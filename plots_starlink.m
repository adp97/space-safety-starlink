%% Representative plots of each Starlink launched up to date
clc; clear all; close all;

code(0); 
load('data.mat');
n_sat = length(satnum_acc);
norad_number = satnum_acc;
for i = 934:n_sat
   norad = norad_number(i);
   code(norad);
   load('data.mat');
%    Starlink.norad.epoch = epoch_acc;
%    Starlink.norad.inc = incl_acc;
%    Starlink.norad.ecc = ecc_acc;
%    Starlink.norad.a = a_acc; 
pause(1)
end