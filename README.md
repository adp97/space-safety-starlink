# Starlink Constallation Orbits Analysis

This repository holds the code developed to analyze the Starlink Constallation, part of the subject Space Safety in the Master in Space Engineering degree. A pdf document can be found too for further details. 

## Getting Started
### Prerequisites

1. MATLAB installed. No toolboxes required 
2. Active account in www.space-track.org

### Setting up 

1. In file 'tle2txt', replace lines 6 and 7 with your user and account, inside the quotations:

```
username='YOUR_USERNAME';
password='YOUR_PASSWORD';
```

## Running the code
The code can be used to:
- Retrieve last TLE information of all Starlink satellites
```
code(0)
```
<div align="center">
    <img src="starlink_plots/starlink_altitude.png" class="center" alt="alt text" width="492" height="369">
</div>

- Retrieve historical TLE from an specific satellite (Input: norad Nº)
```
code('norad')
```
<div align="center">
    <img src="starlink_plots/altitude/45578_altitude.png" class="center" alt="alt text" width="492" height="369">
</div>

- Detect and correct errors in TLE from an specific satellite (Input: norad Nº; tolerance). Several satellites can be retrieved if input is a vector of their norad numbers. 
```
outliers_out(46590,0.05); outliers_out([45752 46551],0.05)
```
![](starlink_plots/altitude/45752_all.png)

- Obtain mean Keplerian coordinates of a satellite. During the three calls explained before, console will output the Keplerian coordinates, as shown here.

<div align="center">
<img width="196" height="176" src="starlink_plots/Keplerian_coords.png">
</div>



## Authors

* **Angel del Pino Jimenez** 
* **Samuel Muñoz Caballero**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
