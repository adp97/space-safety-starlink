function tle2kep(file)
% READTLE Read satellite ephemeris data from a NORAD two-line element (TLE) file.
%
% INPUTS:
%   file    - Path to any standard two-line element file.
%   catalog - Optional array of NORAD catalog numbers for the satellites of
%             interest. The default action is to display data from every
%             satellite in the file.
%
% Brett Pantalone
% North Carolina State University
% Department of Electrical and Computer Engineering
% Optical Sensing Laboratory
% mailto:bapantal@ncsu.edu
% http://research.ece.ncsu.edu/osl/
%
% Modified by Angel del Pino 
% mailto:angel.pino@alumnos.uc3m.es
%

fd = fopen(file,'r');
if fd < 0, fd = fopen([file '.tle'],'r'); end
assert(fd > 0,['Can''t open file ' file ' for reading.'])

j = 0;

A0 = fgetl(fd);

if length(A0) < 69
    A1 = fgetl(fd);
    A2 = fgetl(fd); % length = 69
    while ischar(A2)
        j = j + 1;
        satnum = str2num(A1(3:7));
        fprintf('%s\n', repmat('-',1,50))
        fprintf('Satellite: %s\n', A0)
        assert(chksum(A1), 'Checksum failure on line 1')
        assert(chksum(A2), 'Checksum failure on line 2')
        fprintf('Catalog Number: %d\n', satnum)
        fprintf('Epoch time: %s\n', A1(19:32)) % YYDDD.DDDDDDDD
        fprintf('Keplerian Coordinates:\n')
        incl = str2num(A2(9:16));       % Inclination [deg]
        omega = str2num(A2(18:25));     % Right Ascension of the ascending node [deg]
        ecc = str2num(['.' A2(27:33)]); % Eccentricity
        w = str2num(A2(35:42));         % Argument of perigee (or periapsis) [deg]
        M = str2num(A2(44:51));         % Mean anomaly [deg]
        n = str2num(A2(53:63));         % Mean motion [rev/day]
        T = 86400/n;                    % Period [s]
        a = ((T/(2*pi))^2*398.6e12)^(1/3)/1000; % Semi-major axis [km]
        b = a*sqrt(1-ecc^2);                    % Semi-minor axis [km]
        nu = M2nu(M*pi/180,ecc)*180/pi;  % True anomaly [deg]
        
        fprintf(' - SMA: %.2f km\n', a)
        fprintf(' - ECC: %f\n', ecc)
        fprintf(' - INC: %f deg\n', incl)
        fprintf(' - RAAN: %f deg\n', omega)
        fprintf(' - AOP: %f deg\n', w)
        fprintf(' - TA: %f deg\n', nu)
        
        % Accumulated variables
        incl_acc(j)    =    incl; % Inclination
        omega_acc(j)   =    omega; % RA of ascending node
        ecc_acc(j)     =    ecc; % Eccentricity
        w_acc(j)       =    w; % Argument of perigee
        M_acc(j)       =    M; % Mean anomaly
        n_acc(j)       =    n; % Mean motion
        T_acc(j)       =    T; % Orbital Period
        a_acc(j)       =    a-6371; % Semi-major axis / Altitude
        b_acc(j)       = 	b-6371; % Semi-minor axis / Altitude
        
        satnum_acc(j) = satnum;
        
        %Add here all the variables you want to save
        save('data','incl_acc','ecc_acc','a_acc','satnum','satnum_acc')
        
        A0 = fgetl(fd);
        A1 = fgetl(fd);
        A2 = fgetl(fd);
    end
    fclose(fd);
else
    A1 = fgetl(fd);
    while ischar(A1)
        j = j + 1;
        satnum = str2num(A0(3:7));
        fprintf('%s\n', repmat('-',1,50))
        %fprintf('Satellite: %s\n', A0)
        assert(chksum(A0), 'Checksum failure on line 1')
        %assert(chksum(A1), 'Checksum failure on line 2')
        fprintf('Catalog Number: %d\n', satnum)
        fprintf('Epoch time: %s\n', A0(19:32)) % YYDDD.DDDDDDDD
        fprintf('Keplerian Coordinates:\n')
        epoch = str2num(A0(19:32));     % Epoch [YYDDD.DDDDDDDD]
        incl = str2num(A1(9:16));       % Inclination [deg]
        omega = str2num(A1(18:25));     % Right Ascension of the ascending node [deg]
        ecc = str2num(['.' A1(27:33)]); % Eccentricity
        w = str2num(A1(35:42));         % Argument of perigee (or periapsis) [deg]
        M = str2num(A1(44:51));         % Mean anomaly [deg]
        n = str2num(A1(53:63));         % Mean motion [rev/day]
        T = 86400/n;                    % Period [s]
        a = ((T/(2*pi))^2*398.6e12)^(1/3)/1000; % Semi-major axis [km]
        b = a*sqrt(1-ecc^2);                    % Semi-minor axis [km]
        nu = M2nu(M*pi/180,ecc)*180/pi;  % True anomaly [deg]
        
        fprintf(' - SMA: %.2f km\n', a)
        fprintf(' - ECC: %f\n', ecc)
        fprintf(' - INC: %f deg\n', incl)
        fprintf(' - RAAN: %f deg\n', omega)
        fprintf(' - AOP: %f deg\n', w)
        fprintf(' - TA: %f deg\n', nu)
        
        % Accumulated variables
        epoch_acc(j)   =    epoch; %Epoch time
        incl_acc(j)    =    incl; % Inclination
        omega_acc(j)   =    omega; % RA of ascending node
        ecc_acc(j)     =    ecc; % Eccentricity
        w_acc(j)       =    w; % Argument of perigee
        M_acc(j)       =    M; % Mean anomaly
        n_acc(j)       =    n; % Mean motion
        T_acc(j)       =    T; % Orbital Period
        a_acc(j)       =    a-6371; % Semi-major axis / Altitude
        b_acc(j)       = 	b-6371; % Semi-minor axis / Altitude
        
        satnum_acc(j) = satnum;
        
        %Add here all the variables you want to save
        save('data','incl_acc','ecc_acc','a_acc','satnum','satnum_acc','epoch_acc')
        
        A0 = fgetl(fd);
        A1 = fgetl(fd);
    end
    fclose(fd);
end
end
%%
% Checksum (Modulo 10)
% Letters, blanks, periods, plus signs = 0; minus signs = 1
function result = chksum(str)
result = false; c = 0;

for k = 1:68
    if str(k) > '0' && str(k) <= '9'
        c = c + str(k) - 48;
    elseif str(k) == '-'
        c = c + 1;
    end
end
if mod(c,10) == str(69) - 48
    result = true;
end

end